package ru.pankratov.elevator.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.time.Clock;
import java.time.Instant;

public class ElevatorPositionTest {

	private static final double DELTA = 0.01d;

	@Test
	public void testAltitude() {
		Clock clock = mock(Clock.class);
		Instant init = Instant.now();
		Instant sec50 = init.plusSeconds(50);
		Instant sec75 = init.plusSeconds(75);

		when(clock.instant()).thenReturn(init);
		ElevatorPosition position = new ElevatorPosition(10.0, clock);
		assertEquals(0.0, position.currentAltitude(), DELTA);
		position.onMove(Direction.UP);
		when(clock.instant()).thenReturn(sec50);
		assertEquals(500.0, position.currentAltitude(), DELTA);
		position.onMove(Direction.DOWN);
		when(clock.instant()).thenReturn(sec75);
		assertEquals(250.0, position.currentAltitude(), DELTA);

	}

}
