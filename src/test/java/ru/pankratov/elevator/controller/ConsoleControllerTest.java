package ru.pankratov.elevator.controller;

import org.junit.jupiter.api.Test;

import ru.pankratov.elevator.model.Building;
import ru.pankratov.elevator.model.Elevator;
import ru.pankratov.elevator.model.World;
import ru.pankratov.elevator.view.View;

import static org.mockito.Mockito.*;

public class ConsoleControllerTest {

	Elevator elevator = mock(Elevator.class);
	World world = mock(World.class);
	View view = mock(View.class);

	@Test
	public void testCalls() {
		when(world.getBuilding()).thenReturn(new Building(7, 3.0));
		ConsoleController controller = new ConsoleController(elevator, world, view);
		controller.exec("i5 i7  i5 o3");
		verify(elevator, times(2)).insideCall(5);
		verify(elevator, times(1)).insideCall(7);
		verify(elevator, times(1)).outsideCall(3);
	}

	@Test
	public void testInvalidStrings() {
		when(world.getBuilding()).thenReturn(new Building(7, 3.0));
		ConsoleController controller = new ConsoleController(elevator, world, view);
		controller.exec("");
		verify(view, times(1)).help();
		controller.exec("asdf");
		verify(view, times(2)).help();
		controller.exec("saf43 34");
		verify(view, times(4)).help();

	}

	@Test
	public void testFloorOverflow() {
		when(world.getBuilding()).thenReturn(new Building(7, 3.0));
		ConsoleController controller = new ConsoleController(elevator, world, view);

		controller.exec("i11");
		controller.exec("i15");
		verify(view, times(2)).error("There are only {} floors", 7);

	}
}
