package ru.pankratov.elevator.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import ru.pankratov.elevator.model.Elevator;
import ru.pankratov.elevator.model.World;
import ru.pankratov.elevator.view.View;

public class ConsoleController implements Runnable {
	private BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in));

	private final Elevator elevator;
	private final World world;
	private final int floors;
	private View view;

	public ConsoleController(Elevator elevator, World world, View view) {
		this.elevator = elevator;
		this.world = world;
		this.world.subscribe(view);
		this.view = view;
		this.floors = this.world.getBuilding().getFloors();
	}

	public void execCommand(String cmd) {
		if (cmd.matches("^[io][1-9][0-8]*$")) {
			final Integer floor = Integer.parseInt(cmd.substring(1));
			if(floor > this.floors) {
				view.error("There are only {} floors", this.floors);
			} else {
				switch (cmd.charAt(0)) {
					case 'i':
						elevator.insideCall(floor);
						break;
					case 'o':
						elevator.outsideCall(floor);
						break;
				}
			}
		} else {
			switch (cmd) {
			case "exit":
			case "quit":
			case "q":
				view.exit();
				System.exit(0);
				break;
			default:
				view.help();
			}
		}
	}

	public void exec(String cmds) {
		for (String cmd : cmds.split("\\s+")) {
			execCommand(cmd);
		}
	}

	public void run() {
		view.help();
		while (true) {
			try {
				final String commands = consoleReader.readLine();
				exec(commands);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
