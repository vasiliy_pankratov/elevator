package ru.pankratov.elevator.model;

import java.util.BitSet;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import ru.pankratov.elevator.model.observer.FloorDetectorObserver;

public class SmartElevator implements Elevator, FloorDetectorObserver {

	ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();

	private final ElevatorEngine engine;
	private final int doorsDelayMillis;

	private final BitSet insideCalls;
	private final BitSet outsideCalls;

	private int currentFloor = 1;
	private Direction direction = Direction.UP;

	
	public SmartElevator(ElevatorEngine engine, int floors, int doorsDelayMillis) {
		this.engine = engine;
		this.doorsDelayMillis = doorsDelayMillis;
		this.insideCalls = new BitSet(floors + 1);
		this.outsideCalls = new BitSet(floors + 1);
	}

	private boolean hasCalls(BitSet bitset) {
		return bitset.cardinality() > 0;
	}

	private int maxCall(BitSet bitset) {
		return bitset.previousSetBit(bitset.size() - 1);
	}

	private int minCall(BitSet bitset) {
		return bitset.nextSetBit(0);
	}

	private boolean floorCalled(int floor) {
		return insideCalls.get(floor) || outsideCalls.get(floor);
	}

	private void open() {
		engine.stop();
		engine.openDoors();
		scheduler.schedule(() -> this.close(), doorsDelayMillis, TimeUnit.MILLISECONDS);
	}
	
	private synchronized void close() {
		insideCalls.clear(currentFloor);
		outsideCalls.clear(currentFloor);
		engine.closeDoors();
		nextDirection();
	}

	private void calcDirection(BitSet bitset) {
		if(floorCalled(currentFloor)) {
			open();
		} else if(direction == Direction.UP) {
			int max = maxCall(bitset);
			if(max < currentFloor) {
				direction = Direction.DOWN;
				engine.moveDown();
			} else {
				engine.moveUp();
			}
		} else {
			int min = minCall(bitset);
			if(min > currentFloor) {
				direction = Direction.UP;
				engine.moveUp();
			} else {
				engine.moveDown();
			}
		}
		
	}

	private void nextDirection() {
		if(hasCalls(insideCalls)) {
			calcDirection(insideCalls);
		} else if (hasCalls(outsideCalls)) {
			calcDirection(outsideCalls);
		}
	}

	private void moveIfIdle() {
		if(this.engine.isStopped() && !this.engine.isOpen()) {
			nextDirection();
		}
	}

	@Override
	public synchronized void insideCall(int floor) {
		insideCalls.set(floor);
		moveIfIdle();
	}

	@Override
	public synchronized void outsideCall(int floor) {
		outsideCalls.set(floor);
		moveIfIdle();
	}

	@Override
	public synchronized void onNewFloor(int floor) {
		currentFloor = floor;
		if(floorCalled(currentFloor)) {
			open();
		}
	}

}
