package ru.pankratov.elevator.model;

import ru.pankratov.elevator.model.observer.BasicObservable;
import ru.pankratov.elevator.model.observer.FloorDetectorObserver;

public class FloorDetector extends BasicObservable<FloorDetectorObserver> {
	void newFloor(int floor) {
		notifyObservers((observer) -> observer.onNewFloor(floor));
	}
}
