package ru.pankratov.elevator.model;

import ru.pankratov.elevator.model.observer.BasicObservable;
import ru.pankratov.elevator.model.observer.ElevatorEngineObserver;

public class ElevatorEngine extends BasicObservable<ElevatorEngineObserver> {

	private boolean open = false;
	private Direction currentDirection;
	private boolean stopped = true;

	public ElevatorEngine() {
	}

	public void stop() {
		this.stopped = true;
		notifyObservers(ElevatorEngineObserver::onStop);
	}

	public void openDoors() {
		this.open = true;
		notifyObservers(ElevatorEngineObserver::onOpenDoors);
	}

	public void closeDoors() {
		this.open = false;
		notifyObservers(ElevatorEngineObserver::onCloseDoors);
	}

	private void move(Direction direction) {
		this.stopped = false;
		this.currentDirection = direction;
		notifyObservers((observer) -> observer.onMove(direction));
	}

	public void move() {
		move(this.currentDirection);
	}

	public void moveUp() {
		move(Direction.UP);
	}

	public void moveDown() {
		move(Direction.DOWN);
	}

	public Direction getCurrentDirection() {
		return this.currentDirection;
	}

	public boolean isOpen() {
		return this.open;
	}

	public boolean isStopped() {
		return this.stopped;
	}

}
