package ru.pankratov.elevator.model;

import java.time.Clock;
import java.time.Instant;

import ru.pankratov.elevator.model.observer.ElevatorEngineObserver;

public class ElevatorPosition implements ElevatorEngineObserver {
	private Double elevatorAlt = 0.0;
	private Instant checkpoint;
	private boolean stopped = true;
	private final Double speed;
	private Direction currentDirection;
	private final Clock clock;

	public ElevatorPosition(Double elevatorSpeed) {
		this(elevatorSpeed, Clock.systemUTC());
	}

	public ElevatorPosition(Double elevatorSpeed, Clock clock) {
		this.speed = elevatorSpeed;
		this.clock = clock;
	}

	private Double calcAltitudeTo(Instant ins) {
		Double alt = this.elevatorAlt;
		if (!stopped) {
			final Double time = (ins.toEpochMilli() - checkpoint.toEpochMilli()) / 1000.0;
			final Double distance = time * speed;
			if (currentDirection == Direction.UP) {
				alt += distance;
			} else {
				alt -= distance;
			}
		}
		return alt;
	}

	public Double currentAltitude() {
		return calcAltitudeTo(clock.instant());
	}

	private void onChange() {
		final Instant now = clock.instant();
		this.elevatorAlt = calcAltitudeTo(now);
		checkpoint = now;
	}

	public void onMove(Direction direction) {
		onChange();
		stopped = false;
		currentDirection = direction;
	}

	public void onStop() {
		onChange();
		stopped = true;
	}

}
