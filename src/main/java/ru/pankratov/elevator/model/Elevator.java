package ru.pankratov.elevator.model;

public interface Elevator {

	public abstract void insideCall(int floor);

	public abstract void outsideCall(int floor);

}
