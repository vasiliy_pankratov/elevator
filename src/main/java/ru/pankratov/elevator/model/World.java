package ru.pankratov.elevator.model;

import ru.pankratov.elevator.model.observer.ElevatorEngineObserver;
import ru.pankratov.elevator.model.observer.FloorDetectorObserver;
import ru.pankratov.elevator.model.observer.Observable;
import ru.pankratov.elevator.model.observer.Observer;

public class World implements Runnable, Observable<Observer> {

	private int currentFloor = 1;
	private ElevatorPosition elevatorPosition;
	private final FloorDetector floorDetector = new FloorDetector();
	private final Building building;
	private final double threshold;
	private final ElevatorEngine engine = new ElevatorEngine();

	public World(Building building, double elevatorSpeed) {
		this.building = building;
		this.threshold = this.building.getFloorHeight() / 2.0;
		this.elevatorPosition = new ElevatorPosition(elevatorSpeed);
		this.engine.subscribe(this.elevatorPosition);
	}

	public ElevatorEngine getEngine() {
		return this.engine;
	}
	
	public Building getBuilding() {
		return this.building;
	}
	
	private void checkNewFloor() {
		final int floor = (int) (elevatorPosition.currentAltitude() / building.getFloorHeight()) + 1;
		if (this.currentFloor != floor) {
			this.floorDetector.newFloor(floor);
			this.currentFloor = floor;
		}
	}

	private void checkCrash() {
		if (this.elevatorPosition.currentAltitude() + threshold < 0.0 || this.elevatorPosition.currentAltitude()
				- threshold > (building.getFloors() - 1) * building.getFloorHeight()) {
			throw new RuntimeException("CRASH!!!");
		}
	}

	@Override
	public void run() {
		while (true) {
			checkNewFloor();
			checkCrash();
		}
	}

	@Override
	public void unsubscribe(Observer observer) {
		if (observer instanceof ElevatorEngineObserver) {
			this.engine.unsubscribe((ElevatorEngineObserver) observer);
		}
		if (observer instanceof FloorDetectorObserver) {
			this.floorDetector.unsubscribe((FloorDetectorObserver) observer);
		}
	}

	@Override
	public void subscribe(Observer observer) {
		if (observer instanceof ElevatorEngineObserver) {
			this.engine.subscribe((ElevatorEngineObserver) observer);
		}
		if (observer instanceof FloorDetectorObserver) {
			this.floorDetector.subscribe((FloorDetectorObserver) observer);
		}
	}

}
