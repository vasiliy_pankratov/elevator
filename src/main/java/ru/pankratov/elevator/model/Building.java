package ru.pankratov.elevator.model;

public class Building {
	private final Integer floors;
	private final Double floorHeight;

	public Building(int floors, double floorHeight) {
		this.floors = floors;
		this.floorHeight = floorHeight;
	}

	public double getFloorHeight() {
		return floorHeight;
	}

	public int getFloors() {
		return floors;
	}

	public double height() {
		return floors * floorHeight;
	}

}
