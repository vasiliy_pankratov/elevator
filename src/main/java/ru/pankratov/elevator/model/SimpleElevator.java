package ru.pankratov.elevator.model;

import java.util.ArrayDeque;
import java.util.Optional;
import java.util.Queue;

import ru.pankratov.elevator.model.observer.FloorDetectorObserver;

public class SimpleElevator implements Elevator, FloorDetectorObserver {

	private Queue<Integer> insideCalls = new ArrayDeque<>();
	private Queue<Integer> outsideCalls = new ArrayDeque<>();
	private Optional<Integer> targetFloor = Optional.empty();
	private int currentFloor = 1;
	private final ElevatorEngine engine;
	private final int closeDoorsMillis;
	
	public SimpleElevator(ElevatorEngine engine, int closeDoorsMillis) {
		this.engine = engine;
		this.closeDoorsMillis = closeDoorsMillis;
	}

	private void moveTo(int floor) {
		if (currentFloor < floor) {
			engine.moveUp();
		} else if (currentFloor > floor) {
			engine.moveDown();
		} else {
			atTarget();
		}
	}

	private synchronized void move() {
		if (!targetFloor.isPresent()) {
			if (!insideCalls.isEmpty()) {
				targetFloor = Optional.of(insideCalls.poll());
			} else if (!outsideCalls.isEmpty()) {
				targetFloor = Optional.of(outsideCalls.poll());
			}
			targetFloor.ifPresent((Integer floor) -> moveTo(floor));
		}
	}

	@Override
	public synchronized void insideCall(int floor) {
		insideCalls.add(floor);
		move();
	}

	@Override
	public synchronized void outsideCall(int floor) {
		outsideCalls.add(floor);
		move();
	}

	private void atTarget() {
		engine.stop();
		engine.openDoors();
		try {
			Thread.sleep(closeDoorsMillis);
		} catch (InterruptedException e) {
			// DO NOTHING
		}
		synchronized (this) {
			insideCalls.removeIf((x) -> x == currentFloor);
			outsideCalls.removeIf((x) -> x == currentFloor);
			targetFloor = Optional.empty();
		}
		engine.closeDoors();
		move();
	}

	@Override
	public void onNewFloor(int floor) {
		this.currentFloor = floor;
		if (targetFloor.isPresent() && targetFloor.get() == floor) {
			atTarget();
		}
	}

}
