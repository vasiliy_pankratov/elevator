package ru.pankratov.elevator.model.observer;

public interface FloorDetectorObserver extends Observer {
	default void onNewFloor(int floor) {
	};
}
