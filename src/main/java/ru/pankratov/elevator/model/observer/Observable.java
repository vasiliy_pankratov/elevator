package ru.pankratov.elevator.model.observer;

public interface Observable<T extends Observer> {
	void subscribe(T observer);

	void unsubscribe(T observer);
}
