package ru.pankratov.elevator.model.observer;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class BasicObservable<T extends Observer> implements Observable<T> {

	protected final List<T> observers = new ArrayList<>();

	protected void notifyObservers(Consumer<? super T> func) {
		observers.parallelStream().forEach(func);
	}

	@Override
	public void subscribe(T observer) {
		this.observers.add(observer);
	}

	@Override
	public void unsubscribe(T observer) {
		this.observers.remove(observer);
	}

}
