package ru.pankratov.elevator.model.observer;

import ru.pankratov.elevator.model.Direction;

public interface ElevatorEngineObserver extends Observer {
	default void onStop() {
	};

	default void onOpenDoors() {
	};

	default void onCloseDoors() {
	};

	default void onMove(Direction directioin) {
	};
}
