package ru.pankratov.elevator;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import ru.pankratov.elevator.controller.ConsoleController;
import ru.pankratov.elevator.model.Building;
import ru.pankratov.elevator.model.SmartElevator;
import ru.pankratov.elevator.model.World;
import ru.pankratov.elevator.view.LogView;

public class App {

	private static class Params {
		int floors;
		double floorHeight;
		double elevatorSpeed;
		int doorsDelayMillis;
	}

	private static void checkParams(Params params) {
		if(params.floors < 5 || params.floors > 20) {
			throw new IllegalArgumentException("Floors are supposed to be in range [5, 20]");
		}
		if(params.floorHeight <= 0.0) {
			throw new IllegalArgumentException("Floor height is supposed to be positive");
		}
		if(params.elevatorSpeed <= 0.0) {
			throw new IllegalArgumentException("Elevator speed is supposed to be positive");
		}
		if(params.doorsDelayMillis <= 0.0) {
			throw new IllegalArgumentException("Doors delay is supposed to be positive");
		}

	}

	private static Params parseArgs(String[] args) {
		Params params = new Params();
		params.floors = Integer.parseInt(args[0]);
		params.floorHeight = Double.parseDouble(args[1]);
		params.elevatorSpeed = Double.parseDouble(args[2]);
		params.doorsDelayMillis = (int)(Double.parseDouble(args[2]) * 1000);
		checkParams(params);
		return params;
	}
	public static void main(String[] args) {
		Params params = null;
		try {
			params = parseArgs(args);
		} catch (Exception exception) {
			System.out.println("Expected arguments: \n"
					+ "1) floors number,  int, [5, 20] \n"
					+ "2) floor height (m), double, (0.0, +inf)\n"
					+ "3) lift speed (mps), double, (0.0, +inf)\n"
					+ "4) doors closing delay (s), double, (0.0, +inf)\n\n"
					+ "e.g. 7 3.14 1.57 2.7\n\n"
				);
			System.out.println(exception.toString());
			System.exit(1);
		}

		Executor executor = Executors.newFixedThreadPool(2);
		Building building = new Building(params.floors, params.floorHeight);
		World world = new World(building, params.elevatorSpeed);
		SmartElevator elevator = new SmartElevator(world.getEngine(), building.getFloors(), params.doorsDelayMillis);
		world.subscribe(elevator);
		executor.execute(world);
		
		ConsoleController controller = new ConsoleController(elevator, world, new LogView());
		executor.execute(controller);

	}

}
