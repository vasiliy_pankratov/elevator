package ru.pankratov.elevator.view;

import ru.pankratov.elevator.model.observer.ElevatorEngineObserver;
import ru.pankratov.elevator.model.observer.FloorDetectorObserver;

public interface View extends ElevatorEngineObserver, FloorDetectorObserver {
	void help();
	void error(String message, Object... args);
	void exit();
}
