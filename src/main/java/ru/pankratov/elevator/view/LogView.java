package ru.pankratov.elevator.view;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.pankratov.elevator.model.Direction;

public class LogView implements View {

	private final Logger logger = LoggerFactory.getLogger("ELEVATOR");

	@Override
	public void onStop() {
		logger.info("Stopped");
	}

	@Override
	public void onOpenDoors() {
		logger.info("Open");
	}

	@Override
	public void onCloseDoors() {
		logger.info("Closed");
	}

	@Override
	public void onMove(Direction direction) {
		logger.info("Moving {}", direction);
	}

	@Override
	public void onNewFloor(int floor) {
		logger.info("Floor {}", floor);
	}

	@Override
	public void help() {
		logger.info("You can use following commands (splitted by spaces or new lines):\n" + 
					"\ti9 - inside call 9th floor\n" + 
					"\to7 - outside call 7th floor\n" + 
					"\texit - exit"
				);
	}

	@Override
	public void exit() {
		logger.info("Bye!");
	}

	@Override
	public void error(String message, Object... args) {
		logger.error(message, args);
	}
}
